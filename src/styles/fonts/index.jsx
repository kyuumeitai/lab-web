import IBMPlexMono from './IBMPlexMono-Regular.woff'
import IBMPlexMono2 from './IBMPlexMono-Regular.woff2'

import Apercu from './ApercuPro.eot'
import Apercu2 from './ApercuPro.woff2'

import ApercuProBold from './ApercuPro-Bold.eot'
import ApercuProBold2 from './ApercuPro-Bold.woff2'

export { IBMPlexMono, IBMPlexMono2, Apercu, Apercu2, ApercuProBold, ApercuProBold2 }
