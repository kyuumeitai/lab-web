import { createGlobalStyle } from 'styled-components'

import { IBMPlexMono, IBMPlexMono2, ApercuProBold, ApercuProBold2, Apercu, Apercu2 } from './fonts'

export default createGlobalStyle`
  @font-face {
    font-family: 'IBM Plex Mono';
    src: url(${IBMPlexMono2}) format('woff2'),
          url(${IBMPlexMono}) format('woff');
    font-weight: bold;
    font-style: normal;
  }

  @font-face {
    font-family: 'Apercu';
    src: url(${Apercu2}) format('woff2'),
          url(${Apercu}) format('eot');
    font-weight: normal;
    font-style: normal;
  }

  @font-face {
    font-family: 'Apercu Bold';
    src: url(${ApercuProBold2}) format('woff2'),
          url(${ApercuProBold}) format('eot');
    font-weight: 900;
    font-style: normal;
  }

`
