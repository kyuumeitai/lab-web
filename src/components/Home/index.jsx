import React from 'react'
import foto from '@/images/portada.png'
import arrow from '@/images/arrow.svg'
import styled from 'styled-components'
import scrollTo from 'gatsby-plugin-smoothscroll'

const IframeContainer = styled.div`
    height: calc(80vh - 6rem);
    padding-top: 6rem;
    @media (min-width: 640px){
        height: calc(90vh - 5rem); 
    }
`

const Home = () => {
    return (
        <main className="w-screen max-w-full h-screen grid">
            <IframeContainer id="home"> 
                <img className="w-full h-full" src={foto} alt="temporal" />
            </IframeContainer>
            <div className="w-full h px-6 sm:px-3 grid">
                <h5 className="text-center m-auto font-extrabold sm:max-w-md ">Contamos historias, más allá del slogan, con exhibición en un amplio abanico de medios, que contienen audiencias y nichos para llegar donde se quiere llegar.</h5>
            </div>
            <div className="w-full grid" onClick={()=> scrollTo('#start')}>
                <img className="m-auto w-6 cursor-pointer animate__animated animate__pulse animate__infinite	infinite"  src={arrow} alt="Icono ver más" />
            </div>
        </main>
    )
}

export default Home;