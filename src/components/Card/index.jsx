import React from 'react'
import styled from 'styled-components'

const Container = styled.div`
  display: grid;
  grid-template-rows: 5vh 45vh 7vh auto;
  margin-top: 1.5rem;
  div:last-child{
      &::-webkit-scrollbar{
          display: none;
      }
  }
  @media (min-width: 768px){
    grid-template-rows: 5vh 55vh 7vh 15vh;
  }
`

const Card = (props) => {
    const { marca, year, imagen, filtro, titulo, desc, url } = props;
    const imgQ = imagen.filter(function (el) {
        return el.indexOf(filtro + ".png") > -1
    })

    return (
        <div className="w-auto h-auto"
            style={{ zIndex: 0 }}
            data-sal="fade"
            data-sal-easing="ease-out-back"
            data-sal-duration="2000">
            <a href={url} target="_blank" rel="noopener noreferrer">
                <Container>
                    <div className="px-12 sm:px-0">
                        <label>{marca} | {year}</label>
                    </div>
                    <div className="">
                        <img className="h-full w-full object-cover" src={imgQ} alt={filtro} />
                    </div>
                    <div className="px-12 mt-3 sm:px-0">
                        <h4 className="font-extrabold">{titulo}</h4>
                    </div>
                    <div className="mt-2 sm:mt-0 px-12 sm:px-0 overflow-y-auto">
                        <p className="font-light ">{desc}</p>
                    </div>
                </Container>
            </a>
        </div>
    )
}

export default Card;