import React from 'react'
import logo from '@/images/logo-w.svg'
import logoLT from '@/images/logoLT-white.svg'

const Footer = () => {

    return (
        <footer className="bg-black h-32 max-w-screen w-full grid grid-rows-2 grid-cols-2 md:grid-rows-1 md:grid-cols-4">
            <div className=" grid col-span-2 md:col-start-2 md:col-span-2 md:row-span-2">
                <p className="text-white text-center text-sm m-auto max-w-xs p-0">Apoquindo 4660, Piso 12. Las Condes.
                hola@laboratoriodecontenidos.cl <br/> {new Date().getFullYear()}</p>
            </div>
            <div className="md:row-start-1 grid">
                <img className="m-auto md:my-auto md:ml-12 h-3 sm:h-4" src={logoLT} alt="" />
            </div>
            <div className="grid">
                <img className="m-auto md:my-auto md:mr-12 h-8 sm:h-10" src={logo} alt="" />
            </div>
        </footer>
    )
}

export default Footer