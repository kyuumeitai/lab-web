import React, { useState } from 'react'
import logo from '@/images/logo.svg'
import menuLogo from '@/images/menu.svg'

const Header = () => {

  const [nav, openNav] = useState(false)

  return (
    <header className="bg-white h-20 sm:h-24 sm:flex sm:justify-between sm:flex-row-reverse fixed w-full" style={{zIndex: 5}}>
      <div className="flex items-center justify-between h-full px-12 ">
        <div className="w-auto h-full grid pt-2 sm:hidden" onClick={() => openNav(!nav)}>
          <img src={menuLogo} alt="menu" className="m-auto h-5" />
        </div>
        <div className="w-auto h-full grid pt-2">
          <img src={logo} alt="logo laboratorio de contenido" className="m-auto h-8 sm:h-10" />
        </div>
      </div>

      <div className={(nav ? 'block ' : 'hidden ') + "bg-white pb-4 py-1 sm:flex sm:pb-0 sm:px-12"}>
        <a className="block text-black text-center mt-3 font-semibold hover:text-opacity-50 sm:mt-10 sm:mx-3" href="#">Quiénes Somos</a>
        <a className="block text-black text-center mt-6 font-semibold hover:text-opacity-50 sm:mt-10 sm:mx-3" href="#">Trabajos</a>
        <a className="block text-black text-center mt-6 font-semibold hover:text-opacity-50 sm:mt-10 sm:mx-3" href="#">Contactos</a>
      </div>

    </header>
  )
}

export default Header