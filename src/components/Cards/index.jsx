import React from 'react';
import { useStaticQuery, graphql } from "gatsby"
import styled from 'styled-components'
import Card from '@/components/Card'
import { contenido } from '@/json/index'

const Container = styled.div`
width: 100%;
max-width: 1024px;
display: grid;
grid-gap: 1rem;
grid-template-columns: 1fr;
    @media (min-width: 641px){
        padding: 0 3rem;
        margin: 1.5rem auto;
        grid-template-columns: repeat(2, 1fr);
    }
`
const Cards = () => {

    const datas = useStaticQuery(graphql`
    {
        allFile(filter: {extension: {eq: "png"}}) {
          edges {
            node {
              childImageSharp {
                  fluid {
                    src
                    srcWebp
                    sizes
                  }
                }
              }
            }
          }
        }                
    `
    )

   const imagenes = datas.allFile.edges.map(elem => {
        return elem.node.childImageSharp.fluid.src
    })

    return ( 
        <div id="start" className="pt-16">
        <Container >  
            { contenido.map((item, i) => {return(
                <Card key={i} marca={item.marca} year={item.year} imagen={imagenes} filtro={item.imagen} titulo={item.titulo} desc={item.descripcion} url={item.url}/>
            )})}
        </Container>
        </div>
     );
}
 
export default Cards;