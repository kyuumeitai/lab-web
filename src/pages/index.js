import React from 'react'
import Layout from '@/components/Layout'
import SEO from '@/components/Seo'

import 'animate.css'
import Home from '@/components/Home'
import Cards from '@/components/Cards'
import arrow from '@/images/arrow-up.svg'
import scrollTo from 'gatsby-plugin-smoothscroll'



const IndexPage = () => (
  <Layout>
    <SEO />
    <Home />
    <Cards />
    <div className="w-full grid cursor-pointer my-8" onClick={()=> scrollTo('#home')}>
      <img className="m-auto w-8 " src={arrow} alt="Icono ver más" />
      <p className="text-center font-extrabold mt-2" >Volver</p>
    </div>
  </Layout>
)

export default IndexPage
